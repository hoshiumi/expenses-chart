// const data = require('../data.json')
import * as data from '../data.json'

const measures = document.querySelectorAll('.measure')
const charts = document.querySelector('.charts')
const maxHeight = 150

window.addEventListener('DOMContentLoaded', () => {
    console.log(data.default)

    data.default.forEach((measure) => {
        const currentHeight = (measure.amount * maxHeight) / 52.36 + 'px'
        charts.innerHTML += `<article>
                        <div class="measure" data-measure="${measure.amount}" style="height: ${currentHeight};">
                            <div class="hover-val">${measure.amount}</div>
                        </div>
                        <p class="day">${measure.day}</p>
                    </article>`

        console.log(measure.amount)
    })
})
